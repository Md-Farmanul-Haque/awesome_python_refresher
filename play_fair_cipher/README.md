# Playfair Cipher

The Playfair Cipher is a manual symmetric encryption technique and was the first literal digraph substitution cipher. The technique encrypts pairs of letters (digraphs), instead of single letters as in the simple substitution cipher.

## Table of Contents

- [Introduction](#introduction)
- [History](#history)
- [Algorithm](#algorithm)
  - [Creating the Key Matrix](#creating-the-key-matrix)
  - [Encryption](#encryption)
  - [Decryption](#decryption)
- [Example](#example)
- [Usage](#usage)
- [License](#license)

## Introduction

The Playfair Cipher encrypts a message by using a 5x5 matrix of letters constructed using a keyword. The keyword is used to populate the matrix, with subsequent letters filling the remaining spaces. Since there are 26 letters in the alphabet and only 25 spaces in the matrix, traditionally, 'I' and 'J' are treated as the same letter.

## History

The Playfair cipher was invented by Charles Wheatstone in 1854, but it bears the name of Lord Playfair who promoted its use. It was used by the British during the Boer War and in World War I and II.

## Algorithm

### Creating the Key Matrix

1. Choose a keyword (e.g., "keyword").
2. Remove duplicate letters from the keyword.
3. Fill the 5x5 matrix with the letters of the keyword.
4. Fill the remaining spaces in the matrix with the rest of the alphabet, skipping any duplicate letters already used and treating 'I' and 'J' as the same letter.

### Encryption

1. Divide the plaintext message into digraphs (pairs of letters). If there is an odd number of letters, append an extra letter (commonly 'X') to form the final pair.
2. For each pair of letters:
   - If both letters are the same (or only one letter is left), add an 'X' after the first letter.
   - If the letters appear in the same row of the matrix, replace them with the letters immediately to their right (wrapping around to the start of the row if necessary).
   - If the letters appear in the same column, replace them with the letters immediately below them (wrapping around to the top if necessary).
   - If the letters form a rectangle, replace them with the letters on the same row respectively but at the opposite corners of the rectangle.

### Decryption

Decryption is performed by reversing the encryption process:
1. For each pair of letters in the ciphertext:
   - If the letters appear in the same row, replace them with the letters immediately to their left.
   - If the letters appear in the same column, replace them with the letters immediately above them.
   - If the letters form a rectangle, replace them with the letters on the same row respectively but at the opposite corners of the rectangle.

## Example

### Key Matrix Creation
Keyword: "playfair example"
```
P L A Y F
I R E X M
B C D G H
K N O Q S
T U V W Z
```

### Encryption
Plaintext: "hide the gold"
- Pairing: HI DE TH EG OL DX
- Encrypted Text: BM OD ZB XD NA BE

### Decryption
Ciphertext: "BM OD ZB XD NA BE"
- Decrypted Text: HI DE TH EG OL DX (reverting back to original plaintext, "hide the gold")

## Usage

To use the Playfair Cipher, you can implement it in any programming language. Below is a basic example in Python:

```python
def create_matrix(key):
    # Create a 5x5 matrix with the key
    ...

def encrypt(plaintext, matrix):
    # Encrypt the plaintext using the matrix
    ...

def decrypt(ciphertext, matrix):
    # Decrypt the ciphertext using the matrix
    ...

key = "playfair example"
matrix = create_matrix(key)

plaintext = "hide the gold"
ciphertext = encrypt(plaintext, matrix)
print("Encrypted:", ciphertext)

decrypted_text = decrypt(ciphertext, matrix)
print("Decrypted:", decrypted_text)
```

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for more details.

