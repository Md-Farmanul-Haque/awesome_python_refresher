import pytest
from jailer_problem import jailer_problem

def test_jailer_problem():
    assert jailer_problem() == [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]

if __name__ == "__main__":
    pytest.main()
