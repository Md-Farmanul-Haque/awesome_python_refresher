# Jailer Problem

## Problem Description

A newly constructed jail has 100 cells, numbered from 1 to 100. Initially, all the cells are open. The newly appointed jailer, being bored, decides to walk around the jail and perform the following actions:

1. He starts by closing all the cells, walking from cell number 1 to 100.
2. He then returns to the beginning and visits all even-numbered cells (2, 4, 6, ..., 100), opening the doors of the visited cells.
3. Returning to the beginning again, he visits every third cell (3, 6, 9, ..., 99). He closes the open doors and opens the closed doors.
4. He continues this process, each time visiting every nth cell (4, 8, 12, ..., then 5, 10, 15, ..., and so on up to 100).

After completing 100 rounds, where each round corresponds to visiting every nth cell, we need to determine which doors are open and which are closed.

## Solution

The problem can be solved using a simple simulation. Here's a step-by-step explanation of how the doors' states change:

1. Initially, all doors are open.
2. In the first round, the jailer closes every door.
3. In the second round, the jailer visits every second door (2, 4, 6, ..., 100) and opens them.
4. In the third round, the jailer toggles the state of every third door (3, 6, 9, ..., 99).
5. This process continues until the jailer has completed 100 rounds.

The key observation here is that a door toggles its state (open/close) whenever it is visited. A door is visited in round `k` if `k` is a divisor of the door's number. Thus, the number of times a door is toggled is equal to the number of its divisors.

A door ends up being open if it is toggled an odd number of times, which happens if and only if the door's number is a perfect square (since perfect squares have an odd number of divisors).

Therefore, the doors that remain open are those whose numbers are perfect squares.

## Implementation

The following Python script simulates the jailer's process and determines which doors are open at the end:

```python
def jailer_problem():
    doors = [False] * 100  # False represents a closed door, True represents an open door

    for round in range(1, 101):
        for door in range(round, 101, round):
            doors[door - 1] = not doors[door - 1]

    return [index + 1 for index, door in enumerate(doors) if door]

if __name__ == "__main__":
    open_doors = jailer_problem()
    print(f"The doors that are open are: {open_doors}")
```

### Explanation of the Code

- We initialize a list of 100 boolean values representing the doors, with `False` indicating a closed door.
- For each round from 1 to 100, the jailer toggles the state of every nth door (where `n` is the current round number).
- Finally, we collect and return the door numbers that are open (where the corresponding value in the list is `True`).

## Usage

1. Ensure you have Python installed on your system.
2. Clone this repository:

    ```bash
    git clone https://gitlab.com/Md-Farmanul-Haque/jailer_problem.git
    cd jailer-problem
    ```

3. Run the script:

    ```bash
    python jailer_problem.py
    ```

## Testing

This repository includes a test file `test_jailer_problem.py` to verify the implementation. You can run the tests using `pytest`:

1. Install pytest if you haven't already:

    ```bash
    pip install pytest
    ```

2. Run the tests:

    ```bash
    pytest test_jailer_problem.py
    ```

### Test File

```python
# test_jailer_problem.py

import pytest
from jailer_problem import jailer_problem

def test_jailer_problem():
    assert jailer_problem() == [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]

if __name__ == "__main__":
    pytest.main()
```

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.

## Contributing

Contributions are welcome! Please open an issue or submit a pull request for any changes or improvements.

1. Fork the repository.
2. Create a new branch (`git checkout -b feature-branch`).
3. Make your changes.
4. Commit your changes (`git commit -am 'Add new feature'`).
5. Push to the branch (`git push origin feature-branch`).
6. Open a pull request.

## Acknowledgments

- This implementation is based on the classical problem-solving approach for the Jailer Problem.

Thank you for your interest in this project! If you have any questions, feel free to open an issue or contact the project maintainers.
