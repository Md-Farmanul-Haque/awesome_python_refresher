def jailer_problem() -> list[int]:
    doors = [False] * 100  # False represents a closed door, True represents an open door

    for round in range(1, 101):
        for door in range(round, 101, round):
            doors[door - 1] = not doors[door - 1]

    return [index + 1 for index, door in enumerate(doors) if door]

if __name__ == "__main__":
    open_doors = jailer_problem()
    print(f"The doors that are open are: {open_doors}")

