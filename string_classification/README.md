# String Classification

## Problem Description

This program classifies strings based on the following criteria:

- **Type A**: The string is strictly ascending (e.g., "abcdef").
- **Type D**: The string is strictly descending (e.g., "zyxwv").
- **Type P**: The string is increasing for the first part and descending for the second part (e.g., "abczyx").
- **Type V**: The string is descending for the first part and ascending for the second part (e.g., "zyxabc").
- **Type X**: None of the above.

## Solution

The solution involves writing a function to classify the strings based on the criteria above. We will also include a test file to verify the correctness of our implementation.

### Implementation

The following Python script classifies strings based on the specified criteria:

### `string_classification.py`

```python
def make_pairs(s: str) -> tuple([str, str]):
    return zip(s,s[1:])

def encode(s: str) ->  str:
    def encode_pair(pair: tuple[str, str]):
        if pair[0] < pair[1]:
            return 'A'
        elif pair[0] > pair[1]:
            return 'D'
        else:
            return 'E'
    return ''.join([encode_pair(_) for _ in make_pairs(s)])

def squeeze(s: str) -> str:
    if len(s) == 1:
        return s
    elif s[0] == s[1]:
        return squeeze(s[1:])
    else:
        return s[0] + squeeze(s[1:])
    
def classify_string(s : str) -> str:
    rep = squeeze(encode(s))
    reps = {'A': 'A', 'D': 'D', 'AD':'P','DA':'V'}
    if rep not in reps:
        return 'X'
    else:
        return reps[rep]


if __name__ == "__main__":
    test_strings = ["hello", "zxp", "abczyx", "zyxabc", "mixedstring"]
    for string in test_strings:
        print(f"{string}: {classify_string(string)}")
```

### `test_string_classification.py`

```python
import pytest
from string_classification import classify_string

def test_classify_string():
    assert classify_string("hello") == "Type A"
    assert classify_string("zxp") == "Type D"
    assert classify_string("abczyx") == "Type P"
    assert classify_string("zyxabc") == "Type V"
    assert classify_string("mixedstring") == "Type X"
    assert classify_string("a") == "Type A"  # Single character edge case
    assert classify_string("ab") == "Type A"  # Two characters ascending
    assert classify_string("ba") == "Type D"  # Two characters descending

if __name__ == "__main__":
    pytest.main()
```

### Directory Structure

```
string_classification/
├── string_classification.py
├── test_string_classification.py
└── LICENSE
```

### License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.

### Usage

1. Clone the repository:

    ```bash
    git clone https://gitlab.com/yourusername/string_classification.git
    cd string_classification
    ```

2. Run the classification script:

    ```bash
    python string_classification.py
    ```

3. Run the tests:

    ```bash
    pytest test_string_classification.py
    ```

### License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.

### Contributing

Contributions are welcome! Please open an issue or submit a pull request for any changes or improvements.

1. Fork the repository.
2. Create a new branch (`git checkout -b feature-branch`).
3. Make your changes.
4. Commit your changes (`git commit -am 'Add new feature'`).
5. Push to the branch (`git push origin feature-branch`).
6. Open a pull request.

## Acknowledgments

- This implementation is based on the classical problem-solving approach for string classification.

Thank you for your interest in this project! If you have any questions, feel free to open an issue or contact the project maintainers.

## Example Output

```
hello: Type A
zxp: Type D
abczyx: Type P
zyxabc: Type V
mixedstring: Type X
```
