import unittest


from chess_moves import bishop_moves, rook_moves, knight_moves, queen_moves, check_attack


class TestChessMoves(unittest.TestCase):
    def test_bishop_moves(self):
        self.assertEqual(sorted(bishop_moves('c1')), sorted(['d2', 'e3', 'f4', 'g5', 'h6', 'b2', 'a3']))
        self.assertEqual(sorted(bishop_moves('h8')), sorted(['g7', 'f6', 'e5', 'd4', 'c3', 'b2', 'a1']))


    def test_rook_moves(self):
        self.assertEqual(sorted(rook_moves('a1')), sorted(['a2', 'a3', 'a4', 'a5', 'a6', 'a7', 'a8', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1']))
        self.assertEqual(sorted(rook_moves('h8')), sorted(['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'h7', 'a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8']))

    
    def test_knight_moves(self):
        self.assertEqual(sorted(knight_moves('b1')), sorted(['a3', 'c3', 'd2']))
        self.assertEqual(sorted(knight_moves('g8')), sorted(['h6', 'e7', 'f6']))

    
    def test_queen_moves(self):
        self.assertEqual(sorted(queen_moves('d4')), sorted(bishop_moves('d4') + rook_moves('d4')))
        self.assertEqual(sorted(queen_moves('h1')), sorted(bishop_moves('h1') + rook_moves('h1')))

    
    def test_check_attack(self):
        self.assertEqual(check_attack('queen', 'd4', 'rook', 'h4'), "Queen at position d4 is attacking Rook at position h4")
        self.assertEqual(check_attack('bishop', 'c1', 'knight', 'a3'), "Bishop at position c1 is attacking Knight at position a3")
        self.assertEqual(check_attack('knight', 'b1', 'queen', 'd4'), "Knight at position b1 is attacking Queen at position d4")
        self.assertEqual(check_attack('rook', 'a1', 'bishop', 'h8'), "Rook at position a1 and Bishop at position h8 are not attacking each other")

if __name__ == '__main__':
    unittest.main()

