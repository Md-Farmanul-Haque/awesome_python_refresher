board_size = 8
columns = 'abcdefgh'
rows = '12345678'

def bishop_moves(position: str) -> list[str]:
    col_index = columns.index(position[0]) #getting the index position of the file  
    row_index = rows.index(position[1])#getting the index position of the rank 

    # Generate all possible moves in the four diagonal directions using list comprehensions arranged colckwise
    directions = [(1, 1), (1, -1),(-1, -1), (-1, 1)]

    bishop_attacking_positions = [
        columns[new_col_index] + rows[new_row_index]
        for col_step, row_step in directions
        for new_col_index, new_row_index in 
            [(col_index + i * col_step, row_index + i * row_step) for i in range(1, board_size)]
        if 0 <= new_col_index < board_size and 0 <= new_row_index < board_size
    ]

    return bishop_attacking_positions


def rook_moves(position: str) -> list[str]:
    col_index = columns.index(position[0])
    row_index = rows.index(position[1])

    directions = [(1, 0), (0, -1), (-1, 0), (0, 1)]

    rook_attacking_positions = [
        columns[col_index + i * col_step] + rows[row_index + i * row_step]
        for col_step, row_step in directions
        for i in range(1, board_size)
        if 0 <= col_index + i * col_step < board_size and 0 <= row_index + i * row_step < board_size
    ]

    return rook_attacking_positions

def knight_moves(position: str) -> list[str] :
    col_index = columns.index(position[0])
    row_index = rows.index(position[1])

    # Generate all possible moves in L-shape using list comprehensions
    moves = [
        (2, 1), (2, -1), (-2, -1), (-2, 1),  # Horizontal moves with vertical offset
        (1, 2), (1, -2), (-1, -2), (-1, 2)   # Vertical moves with horizontal offset
    ]

    knight_attacking_positions = [
        columns[col_index + col_step] + rows[row_index + row_step]
        for col_step, row_step in moves
        if 0 <= col_index + col_step < board_size and 0 <= row_index + row_step < board_size
    ]

    return knight_attacking_positions

def queen_moves(position: str) -> list[str]:
    return bishop_moves(position) + rook_moves(position)

def check_attack(piece1: str, position1: str, piece2: str, position2: str) -> str:
    move_functions = {'bishop': bishop_moves, 'rook': rook_moves, 'knight': knight_moves, 'queen': queen_moves}
    attacking_positions1 = move_functions[piece1](position1)
    attacking_positions2 = move_functions[piece2](position2)

    if position1 in attacking_positions2 and position2 in attacking_positions1:
        return f"{piece1.capitalize()} at position {position1} and {piece2.capitalize()} at position {position2} are attacking each other"
    elif position1 in attacking_positions2:
        return f"{piece1.capitalize()} at position {position1} is being attacked by {piece2.capitalize()} at position {position2}"
    elif position2 in attacking_positions1:
        return f"{piece1.capitalize()} at position {position1} is attacking {piece2.capitalize()} at position {position2}"
    else:
        return f"{piece1.capitalize()} at position {position1} and {piece2.capitalize()} at position {position2} are not attacking each other"

piece1 = 'queen'
position1 = 'a5'
piece2 = 'knight'
position2 = 'c6'

are_attacking = check_attack(piece1, position1, piece2, position2)
print(are_attacking)
