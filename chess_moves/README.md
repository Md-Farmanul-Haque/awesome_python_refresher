# Chess Piece Moves

## Overview

This Python module provides functionality to determine the possible moves of chess pieces and check if two given pieces are attacking each other on an 8x8 chess board. The supported pieces include the bishop, rook, knight, and queen.

## Features

- Calculate all possible moves for a bishop, rook, knight, or queen from a given position.
- Determine if one piece is attacking another piece based on their positions.
- Check if two pieces are attacking each other.

## Usage

### Functions

1. **`bishop_moves(position: str) -> list[str]`**
   - Generates all possible moves for a bishop from the given position.
   - **Parameters:**
     - `position`: A string representing the bishop's position on the board (e.g., "a5").
   - **Returns:**
     - A list of strings representing all positions the bishop can move to.

2. **`rook_moves(position: str) -> list[str]`**
   - Generates all possible moves for a rook from the given position.
   - **Parameters:**
     - `position`: A string representing the rook's position on the board (e.g., "a5").
   - **Returns:**
     - A list of strings representing all positions the rook can move to.

3. **`knight_moves(position: str) -> list[str]`**
   - Generates all possible moves for a knight from the given position.
   - **Parameters:**
     - `position`: A string representing the knight's position on the board (e.g., "a5").
   - **Returns:**
     - A list of strings representing all positions the knight can move to.

4. **`queen_moves(position: str) -> list[str]`**
   - Generates all possible moves for a queen from the given position by combining the moves of a bishop and a rook.
   - **Parameters:**
     - `position`: A string representing the queen's position on the board (e.g., "a5").
   - **Returns:**
     - A list of strings representing all positions the queen can move to.

5. **`check_attack(piece1: str, position1: str, piece2: str, position2: str) -> str`**
   - Checks if two given pieces are attacking each other based on their positions.
   - **Parameters:**
     - `piece1`: A string representing the type of the first piece (e.g., "queen").
     - `position1`: A string representing the position of the first piece (e.g., "a5").
     - `piece2`: A string representing the type of the second piece (e.g., "knight").
     - `position2`: A string representing the position of the second piece (e.g., "c6").
   - **Returns:**
     - A string message indicating the attack status between the two pieces.

### Example

```python
piece1 = 'queen'
position1 = 'a5'
piece2 = 'knight'
position2 = 'c6'

are_attacking = check_attack(piece1, position1, piece2, position2)
print(are_attacking)
```

This example checks if a queen at position "a5" and a knight at position "c6" are attacking each other and prints the result.

## Installation

To use this module, simply download the script and import the functions into your Python project.

```python
from chess_piece_attack_checker import bishop_moves, rook_moves, knight_moves, queen_moves, check_attack
```

## License

This project is licensed under the MIT [LICENSE](LICENSE).

## Contributing

Contributions are welcome! If you find any bugs or have suggestions for improvements, please create an issue or submit a pull request.

## Contact

For any questions or inquiries, please contact [farmanhaque74@gmail.com](mailto:farmanhaque74@gmail.com).

---

