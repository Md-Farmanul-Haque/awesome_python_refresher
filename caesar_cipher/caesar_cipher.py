import sys

def caesar_cipher(text, shift):
    return ''.join(
        chr(((ord(char) - 65 + shift) % 26) + 65) if char.isupper() else
        chr(((ord(char) - 97 + shift) % 26) + 97) if char.islower() else
        char
        for char in text
    )


def encrypt(text, shift):
    return caesar_cipher(text, shift)


def decrypt(text, shift):
    return caesar_cipher(text, -shift)


def main():
    if len(sys.argv) != 4:
        print("Usage: python caesar_cipher.py <encrypt|decrypt> <message> <shift>")
        sys.exit(1)
    
    operation = sys.argv[1]
    message = sys.argv[2]
    shift = int(sys.argv[3])

    if operation == "encrypt":
        result = encrypt(message, shift)
        print("Encrypted:", result)

    elif operation == "decrypt":
        result = decrypt(message, shift)
        print("Decrypted:", result)
   
    else:
        print("Invalid operation. Use 'encrypt' or 'decrypt'.")
        sys.exit(1)


if __name__ == "__main__":
    main()
