import unittest
from caesar_cipher import encrypt, decrypt

class TestCaesarCipher(unittest.TestCase):
    
    def test_encrypt(self):
        self.assertEqual(encrypt("Hello World", 3), "Khoor Zruog")
        self.assertEqual(encrypt("abc", 1), "bcd")
        self.assertEqual(encrypt("XYZ", 2), "ZAB")
        self.assertEqual(encrypt("123", 5), "123")
        self.assertEqual(encrypt("Hello, World!", 5), "Mjqqt, Btwqi!")
    
    def test_decrypt(self):
        self.assertEqual(decrypt("Khoor Zruog", 3), "Hello World")
        self.assertEqual(decrypt("bcd", 1), "abc")
        self.assertEqual(decrypt("ZAB", 2), "XYZ")
        self.assertEqual(decrypt("123", 5), "123")
        self.assertEqual(decrypt("Mjqqt, Btwqi!", 5), "Hello, World!")

if __name__ == '__main__':
    unittest.main()
