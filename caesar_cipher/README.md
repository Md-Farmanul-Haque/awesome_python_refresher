# Caesar Cipher

### This project implements a simple Caesar cipher for encrypting and decrypting messages. The Caesar cipher shifts each letter in the input text by a specified number of positions in the alphabet.

## Files
```markdown
- `caesar_cipher.py`: Contains the functions to encrypt and decrypt messages using the Caesar cipher.
- `test_caesar_cipher.py`: Contains unit tests for the encryption and decryption functions.
- `requirements.txt` : Contains requirements to run the caesar_cipher.py and test_caesar_cipher.py.
```
## Requirements
```
- Python 3.x
- `pytest` (for running the tests)
```

## Installation

1. **Clone the repository**:
    ```sh
    git clone <repository_url>
    cd <repository_directory>
    ```

2. **Install `pytest`**:
    ```sh
    pip install pytest
    ```

## Usage

### Encrypt a Message

To encrypt a message, use the `encrypt` function in `caesar_cipher.py`.

Example:
```python
from caesar_cipher import encrypt

message = "Hello World"
shift = 3
encrypted_message = encrypt(message, shift)
print("Encrypted:", encrypted_message)
```

### Decrypt a Message

To decrypt a message, use the `decrypt` function in `caesar_cipher.py`.

Example:
```python
from caesar_cipher import decrypt

encrypted_message = "Khoor Zruog"
shift = 3
decrypted_message = decrypt(encrypted_message, shift)
print("Decrypted:", decrypted_message)
```

### Command-Line Interface

You can also run the script from the command line to encrypt or decrypt a message:

```sh
python caesar_cipher.py <encrypt/decrypt> <message> <shift>
```

Example:
```sh
python caesar_cipher.py encrypt "Hello World" 3
python caesar_cipher.py decrypt "Khoor Zruog" 3
```

## Testing

Unit tests are provided in `test_caesar_cipher.py` and can be run using `pytest`.

To run the tests:
1. Navigate to the project directory.
2. Run `pytest`:
    ```sh
    pytest test_caesar_cipher.py
    ```

### Test Cases

The tests include various scenarios to ensure the correctness of the encryption and decryption functions.

Example test case:
```python
from caesar_cipher import encrypt, decrypt

def test_encrypt():
    assert encrypt("Hello World", 3) == "Khoor Zruog"
    assert encrypt("abc", 1) == "bcd"
    assert encrypt("XYZ", 2) == "ZAB"
    assert encrypt("123", 5) == "123"
    assert encrypt("Hello, World!", 5) == "Mjqqt, Btwqi!"

def test_decrypt():
    assert decrypt("Khoor Zruog", 3) == "Hello World"
    assert decrypt("bcd", 1) == "abc"
    assert decrypt("ZAB", 2) == "XYZ"
    assert decrypt("123", 5) == "123"
    assert decrypt("Mjqqt, Btwqi!", 5) == "Hello, World!"
```

## Example Code

**caesar_cipher.py**:
```python
def caesar_cipher(text, shift):
    return ''.join(
        chr(((ord(char) - 65 + shift) % 26) + 65) if char.isupper() else
        chr(((ord(char) - 97 + shift) % 26) + 97) if char.islower() else char
        for char in text
    )

def encrypt(text, shift):
    return caesar_cipher(text, shift)

def decrypt(text, shift):
    return caesar_cipher(text, -shift)

if __name__ == "__main__":
    import sys
    if len(sys.argv) != 4:
        print("Usage: python caesar_cipher.py <encrypt/decrypt> <message> <shift>")
        sys.exit(1)
    
    command = sys.argv[1]
    message = sys.argv[2]
    shift = int(sys.argv[3])
    
    if command == "encrypt":
        print("Encrypted:", encrypt(message, shift))
    elif command == "decrypt":
        print("Decrypted:", decrypt(message, shift))
    else:
        print("Invalid command. Use 'encrypt' or 'decrypt'.")
        sys.exit(1)
```

**test_caesar_cipher.py**:
```python
from caesar_cipher import encrypt, decrypt

def test_encrypt():
    assert encrypt("Hello World", 3) == "Khoor Zruog"
    assert encrypt("abc", 1) == "bcd"
    assert encrypt("XYZ", 2) == "ZAB"
    assert encrypt("123", 5) == "123"
    assert encrypt("Hello, World!", 5) == "Mjqqt, Btwqi!"

def test_decrypt():
    assert decrypt("Khoor Zruog", 3) == "Hello World"
    assert decrypt("bcd", 1) == "abc"
    assert decrypt("ZAB", 2) == "XYZ"
    assert decrypt("123", 5) == "123"
    assert decrypt("Mjqqt, Btwqi!", 5) == "Hello, World!"
```

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for more details.


