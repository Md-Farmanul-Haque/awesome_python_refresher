# Sieve of Eratosthenes

This repository contains a Python implementation of the Sieve of Eratosthenes algorithm. The Sieve of Eratosthenes is an efficient way to find all prime numbers up to a given limit. This implementation is written in a functional programming style using small, well-defined functions.

## Table of Contents

- [Introduction](#introduction)
- [Features](#features)
- [Installation](#installation)
- [Usage](#usage)
- [Testing](#testing)
- [License](#license)
- [Contributing](#contributing)

## Introduction

The Sieve of Eratosthenes is a classical algorithm used to find all prime numbers up to a specified integer. It works by iteratively marking the multiples of each prime number starting from 2. The numbers that remain unmarked at the end of the algorithm are prime.

## Features

- Functional programming style.
- Efficiently finds all prime numbers up to a given limit.
- Simple and easy-to-understand code structure.
- Includes test files to verify the implementation.

## Installation

To get started, clone the repository:

```bash
git clone https://gitlab.com/Md-Farmanul-Haque/sieve_of_eratosthenese.git
cd sieve_of_eratosthenes
```

Ensure you have Python 3 installed on your system. You can check your Python version by running:

```bash
python --version
```

## Usage

You can run the Sieve of Eratosthenes implementation by executing the `sieve.py` file. The following is an example usage:

```python
# sieve.py

def create_number_list(limit: int) -> list:
    """Create a list of numbers from 2 to limit."""
    return list(range(2, limit + 1))

def mark_non_primes(numbers: int) -> int:
    """Mark non-prime numbers by setting them to None using list comprehension."""
    for i in range(len(numbers)):
        if numbers[i] is not None:
            numbers = [
                None if j != i and numbers[j] is not None and numbers[j] % numbers[i] == 0 else numbers[j]
                for j in range(len(numbers))
            ]
    return numbers

def collect_primes(numbers: int) -> list:
    """Collect numbers that are not marked as None (i.e., primes)."""
    return [num for num in numbers if num is not None]

def sieve_of_eratosthenes(limit: int) -> list:
    """Main function to execute the Sieve of Eratosthenes."""
    numbers = create_number_list(limit)
    marked_numbers = mark_non_primes(numbers)
    primes = collect_primes(marked_numbers)
    return primes

# Example usage:
if __name__ == "__main__":
    limit = 50
    primes = sieve_of_eratosthenes(limit)
    print(f"Prime numbers up to {limit}: {primes}")
```

To run the script, execute:

```bash
python sieve.py
```

## Testing

This repository includes a test file `test_sieve.py` to verify the implementation. You can run the tests using `pytest`:

1. Install pytest if you haven't already:

    ```bash
    pip install pytest
    ```

2. Run the tests:

    ```bash
    pytest test_sieve.py
    ```

The `test_sieve.py` file:

```python
# test_sieve.py

from sieve import sieve_of_eratosthenes

def test_sieve_of_eratosthenes():
    assert sieve_of_eratosthenes(10) == [2, 3, 5, 7]
    assert sieve_of_eratosthenes(20) == [2, 3, 5, 7, 11, 13, 17, 19]
    assert sieve_of_eratosthenes(1) == []
    assert sieve_of_eratosthenes(2) == [2]
    assert sieve_of_eratosthenes(0) == []
```

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.

## Contributing

Contributions are welcome! Please open an issue or submit a pull request for any changes or improvements.

1. Fork the repository.
2. Create a new branch (`git checkout -b feature-branch`).
3. Make your changes.
4. Commit your changes (`git commit -am 'Add new feature'`).
5. Push to the branch (`git push origin feature-branch`).
6. Open a pull request.

## Acknowledgments

- This implementation is based on the classical Sieve of Eratosthenes algorithm.

Thank you for your interest in this project! If you have any questions, feel free to open an issue or contact the project maintainers.

---
