def create_number_list(limit: int) -> list[int]:
    """Create a list of numbers from 2 to limit."""
    return list(range(2, limit + 1))

def mark_non_primes(numbers: int) -> list[int]:
    """Mark non-prime numbers by setting them to None using list comprehension."""
    for i in range(len(numbers)):
        if numbers[i] is not None:
            numbers = [
                None if j != i and numbers[j] is not None and numbers[j] % numbers[i] == 0 else numbers[j]
                for j in range(len(numbers))
            ]
    return numbers

def collect_primes(numbers: int) -> list[int]:
    """Collect numbers that are not marked as None (i.e., primes)."""
    return [num for num in numbers if num is not None]

def sieve_of_eratosthenes(limit: int) -> list[int]:
    """Main function to execute the Sieve of Eratosthenes."""
    numbers = create_number_list(limit)
    marked_numbers = mark_non_primes(numbers)
    primes = collect_primes(marked_numbers)
    return primes

# Example usage:
limit = 50
primes = sieve_of_eratosthenes(limit)
print(f"Prime numbers up to {limit}: {primes}")
