# test_knight_moves.py

import unittest
from knight_moves import min_moves_to_cover_board

class TestKnightMoves(unittest.TestCase):

    def test_min_moves_to_cover_board(self):
        self.assertEqual(min_moves_to_cover_board(0, 0), 63)
        self.assertEqual(min_moves_to_cover_board(2, 2), 24)
        self.assertEqual(min_moves_to_cover_board(1, 1), 6)
        self.assertEqual(min_moves_to_cover_board(4, 4), 28)

if __name__ == '__main__':
    unittest.main()
