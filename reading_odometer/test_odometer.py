import unittest
from odometer import Odometer

class TestOdometer(unittest.TestCase):
    def test_next_reading(self):
        odometer = Odometer(123)
        self.assertEqual(odometer.next_reading(), 124)
        odometer = Odometer(129)
        self.assertEqual(odometer.next_reading(), 134)

    def test_previous_reading(self):
        odometer = Odometer(124)
        self.assertEqual(odometer.previous_reading(), 123)
        odometer = Odometer(234)
        self.assertEqual(odometer.previous_reading(), 189)

    def test_next_reading_with_steps(self):
        odometer = Odometer(123)
        self.assertEqual(odometer.next_reading_with_steps(1), 124)
        self.assertEqual(odometer.next_reading_with_steps(5), 129)
        odometer = Odometer(129)
        self.assertEqual(odometer.next_reading_with_steps(1), 134)
        self.assertEqual(odometer.next_reading_with_steps(2), 135)

    def test_previous_reading_with_steps(self):
        odometer = Odometer(124)
        self.assertEqual(odometer.previous_reading_with_steps(1), 123)
        self.assertEqual(odometer.previous_reading_with_steps(5), 119)
        odometer = Odometer(134)
        self.assertEqual(odometer.previous_reading_with_steps(1), 129)
        self.assertEqual(odometer.previous_reading_with_steps(2), 124)

    def test_steps_between_readings(self):
        self.assertEqual(Odometer.steps_between_readings(123, 124), 1)
        self.assertEqual(Odometer.steps_between_readings(123, 129), 5)
        self.assertEqual(Odometer.steps_between_readings(123, 134), 11)
        with self.assertRaises(ValueError):
            Odometer.steps_between_readings(123, 1234)

if __name__ == '__main__':
    unittest.main()

