class Odometer:
    def __init__(self, reading: int):
        self._reading = reading  # Use a leading underscore to indicate 'private' variable

    @property
    def reading(self) -> int:
        return self._reading

    @staticmethod
    def size(reading: int) -> int:
        return len(str(reading))

    @staticmethod
    def get_limits(reading: int) -> tuple[int, int]:
        DIGITS = '123456789'
        k = Odometer.size(reading)
        return int(DIGITS[:k]), int(DIGITS[-k:])

    @staticmethod
    def is_ascending(reading: int) -> bool:
        digits = str(reading)
        return all(digits[i] < digits[i + 1] for i in range(len(digits) - 1))

    def next_reading(self) -> int:
        start, limit = Odometer.get_limits(self.reading)
        if self.reading == limit:
            return start
        reading = self.reading + 1
        while not Odometer.is_ascending(reading):
            reading += 1
        return reading

    def previous_reading(self) -> int:
        start, limit = Odometer.get_limits(self.reading)
        if self.reading == start:
            return limit
        reading = self.reading - 1
        while not Odometer.is_ascending(reading):
            reading -= 1
        return reading

    def next_reading_with_steps(self, steps: int = 1) -> 'Odometer':
        reading = self.reading
        for _ in range(steps):
            reading = Odometer(reading).next_reading()
        return Odometer(reading)

    def previous_reading_with_steps(self, steps: int) -> 'Odometer':
        start, limit = Odometer.get_limits(self.reading)
        reading = self.reading
        for _ in range(steps):
            if reading == start:
                reading = limit
            else:
                reading -= 1
                while not Odometer.is_ascending(reading):
                    reading -= 1
        return Odometer(reading)

    @staticmethod
    def steps_between_readings(a_reading: int, b_reading: int) -> int:
        if Odometer.size(a_reading) != Odometer.size(b_reading):
            raise ValueError("Readings must have the same number of digits.")
        steps = 0
        current_reading = a_reading
        while current_reading != b_reading:
            steps += 1
            current_reading = Odometer(current_reading).next_reading()
        return steps

# Example usage:
odometer = Odometer(1234)
print(odometer.next_reading())  # Output: 1235 (or the next valid ascending reading)
print(odometer.previous_reading())  # Output: 1233 (or the previous valid ascending reading)
new_odometer = odometer.next_reading_with_steps(5)  # Create a new instance
print(new_odometer.reading)  # Output: the reading 5 steps after 1234
previous_odometer = odometer.previous_reading_with_steps(5)  # Create a new instance
print(previous_odometer.reading)  # Output: the reading 5 steps before 1234
print(Odometer.steps_between_readings(1234, 1245))  # Output: number of steps from 1234 to 1245
