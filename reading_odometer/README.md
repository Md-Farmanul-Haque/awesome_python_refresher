# Odometer Problem

This project involves solving the Odometer problem with specific constraints and functionalities. The odometer in this problem has the following characteristics:
- It does not have zero in its readings.
- The readings of the odometer are in strict ascending order.
- It has a fixed number of dial(s) which can be 3, 4, 5, 6, 7, etc.

## Functions

This project provides the following functions:

1. **Next Reading**
   - Given a reading, return the next reading.
   - Function signature: `next_reading(current_reading: str) -> str`

2. **Previous Reading**
   - Given a reading, return the previous reading.
   - Function signature: `previous_reading(current_reading: str) -> str`

3. **Next Reading with Steps**
   - Given a reading and a step count, return the reading after the given number of steps.
   - Function signature: `next_reading_with_steps(current_reading: str, steps: int) -> str`

4. **Previous Reading with Steps**
   - Given a reading and a step count, return the reading before the given number of steps.
   - Function signature: `previous_reading_with_steps(current_reading: str, steps: int) -> str`

5. **Steps Between Readings**
   - Given two readings, return the number of steps to reach from the first reading to the second reading.
   - Function signature: `steps_between_readings(reading1: str, reading2: str) -> int`

## Installation

To use these functions, simply include the Python script in your project. There are no external dependencies required.

## Usage

Useage of functions provided:

### Example

```python
from odometer import next_reading, previous_reading, next_reading_with_steps, previous_reading_with_steps, steps_between_readings

# Example readings
reading = "129"
steps = 2

# Get the next reading
print(next_reading(reading))  # Output: "134"

# Get the previous reading
print(previous_reading(reading))  # Output: "128"

# Get the reading after a given number of steps
print(next_reading_with_steps(reading, steps))  # Output: "135"

# Get the reading before a given number of steps
print(previous_reading_with_steps(reading, steps))  # Output: "127"

# Get the number of steps between two readings
print(steps_between_readings("123", "134"))  # Output: 7
```

## Function Details

### `next_reading(current_reading: str) -> str`
This function calculates the next reading of the odometer given the current reading. It handles the constraint of no zero in the readings.

### `previous_reading(current_reading: str) -> str`
This function calculates the previous reading of the odometer given the current reading, maintaining the ascending order and no zero constraint.

### `next_reading_with_steps(current_reading: str, steps: int) -> str`
This function returns the reading after a given number of steps from the current reading.

### `previous_reading_with_steps(current_reading: str, steps: int) -> str`
This function returns the reading before a given number of steps from the current reading.

### `steps_between_readings(reading1: str, reading2: str) -> int`
This function calculates the number of steps needed to reach from `reading1` to `reading2`.

## Contributing

Feel free to fork this repository, make changes, and submit pull requests. For major changes, please open an issue first to discuss what you would like to change.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for more details.

---
