# **ISBN-13 Validator**

This project provides a utility to validate ISBN-13 numbers. It ensures that the given ISBN-13 number is correct by calculating and verifying its check digit.

## Features

```markdown
- Validate ISBN-13 numbers by verifying the check digit.
- Simple and easy-to-use command-line interface.
```
## Installation

To use the ISBN-13 Validator, you need to have Python installed. You can download Python from [python.org](https://www.python.org/).

1. Clone the repository:
    ```sh
    git clone https://github.com/your-username/isbn-13-validator.git
    cd isbn-13-validator
    ```

2. No additional packages are required for this project.

## Usage

Run the validator using the following command:

```sh
python isbn13_validator.py
```

You will be prompted to enter an ISBN-13 number. The program will then validate the number and display whether it is valid or invalid.

## Example

```sh
$ python isbn13_validator.py
Enter the ISBN-13 number: 978-04-700-5902-9
The ISBN-13 number is valid.
```

## Project Structure

- `isbn13_validator.py`: Main script to validate ISBN-13 numbers.
- `README.md`: Project documentation.

## Functions

### `clean(isbn: str) -> list[int]`
Removes the HYPHEN "-" from the given ISBN13 number and returns a list of intgeres.

### `validate_isbn13(isbn: str) -> bool`
Validates the ISBN-13 number by checking if the provided check digit matches the calculated check digit.

### `validate_digits(isbn: str) -> int:`
Validates the weighted sum of digit returns 0 if the operations results in 0 or (10 - weighted\_sum of digits) if result != 0

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for more details.


