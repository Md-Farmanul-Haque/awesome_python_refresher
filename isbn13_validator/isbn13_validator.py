from typing import List

HYPHEN = "-"

def clean_isbn(isbn: str) -> List[int]:
    return [int(ch) for ch in isbn if ch != HYPHEN]

def calculate_check_digit(isbn: str) -> int:
    weights = [1, 3] * 6
    cleaned_isbn = clean_isbn(isbn)
    weighted_sum = sum(a * b for a, b in zip(weights, cleaned_isbn))
    remainder = weighted_sum % 10
    return 0 if remainder == 0 else 10 - remainder

def check_isbn(isbn: str) -> bool:
    isbn_body = isbn[:-1]
    actual_check_digit = int(isbn[-1])
    calculated_check_digit = calculate_check_digit(isbn_body)
    return calculated_check_digit == actual_check_digit


ISBN = "978-3-16-148410-0"
is_valid = check_isbn(ISBN)
print(f"Is {ISBN} valid? {is_valid}")
