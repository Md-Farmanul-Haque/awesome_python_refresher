import unittest
from isbn13_validator import clean, check_digit, check_isbn

class TestISBN13Validator(unittest.TestCase):

    def test_clean(self):
        self.assertEqual(clean("978-3-16-148410-0"), [9, 7, 8, 3, 1, 6, 1, 4, 8, 4, 1, 0])
        self.assertEqual(clean("9783161484100"), [9, 7, 8, 3, 1, 6, 1, 4, 8, 4, 1, 0])
        self.assertEqual(clean("0-19-852663-6"), [0, 1, 9, 8, 5, 2, 6, 6, 3, 6])

    def test_calculate_check_digit(self):
        self.assertEqual(check_digit("978-3-16-148410-"), 0)
        self.assertEqual(check_digit("978-0-306-40615-"), 7)
        self.assertEqual(check_digit("978-1-4028-9462-"), 7)

    def test_check_isbn(self):
        self.assertTrue(check_isbn("978-3-16-148410-0"))
        self.assertTrue(check_isbn("978-0-306-40615-7"))
        self.assertTrue(check_isbn("978-1-4028-9462-6"))
        self.assertFalse(check_isbn("978-3-16-148410-1"))
        self.assertFalse(check_isbn("978-0-306-40615-8"))
        self.assertFalse(check_isbn("978-1-4028-9462-7"))

if __name__ == '__main__':
    unittest.main()
